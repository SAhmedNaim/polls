<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/** @var TYPE_NAME $router */
$router->group([
    'prefix'    => 'api/v1',
], function () use ($router) {

    $router->get('/', 'ExampleController@index');

    // User Authenticate
    $router->post('/login', 'UsersController@authenticate');

    // Users Resource
    $router->post('/users', 'UsersController@create');

    $router->group(['middleware' => 'auth:api'], function () use ($router) {
        $router->get('/users', 'UsersController@index');
        $router->get('/me', 'UsersController@me');
    });




});




